package com.qi.demo.Thread;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @description:
 * @author: qi
 * @create: 2019-02-18 13:55
 **/
public class ConditionDemo {

    static   volatile  int data=0;
  static   ReentrantLock  reentrantLock=new ReentrantLock();

  static Condition  condition=reentrantLock.newCondition();
    public static void main(String[] args) {


        new  Thread(){

            @Override
            public void run() {
                System.out.println("第一个线程拿到锁");
                reentrantLock.lock();
                try {
                    System.out.println("第一个线程 进入阻塞队列等待 释放锁");
                    condition.await();
                    System.out.println("第一个线程被唤醒 拿到锁 继续执行");
                }catch (Exception e){

                   e.printStackTrace();
                }
                System.out.println("第一个线程 完成 释放锁");
                reentrantLock.unlock();


            }
        }.start();


        new  Thread(){

            @Override
            public void run() {
                System.out.println("第二个线程 拿到锁");
                reentrantLock.lock();
                System.out.println("第二个线程 去唤醒第一个线程");
          condition.signal();
                System.out.println("第二个线程 释放锁");
                reentrantLock.unlock();

            }
        }.start();




    }
}
