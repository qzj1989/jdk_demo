package com.qi.demo.Thread;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @description:
 * @author: qi
 * @create: 2019-02-20 09:55
 **/
public class ReentrantReadWriteLockDemo {


    public static void main(String[] args) {


        ReentrantReadWriteLock  readWriteLock=new ReentrantReadWriteLock();
        readWriteLock.writeLock().lock();

//        readWriteLock.writeLock().unlock();

        readWriteLock.readLock().lock();

//        readWriteLock.readLock().unlock();


    }

}
