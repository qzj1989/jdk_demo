package com.qi.demo.Thread;

import java.util.concurrent.Semaphore;

/**
 * @description:
 * @author: qi
 * @create: 2019-02-21 14:12
 **/
public class SemaphoreDemo {


    public static void main(String[] args)  throws   Exception {

        Semaphore  semaphore=new Semaphore(0);


        new Thread(){
            @Override
            public void run() {

                try {
                    Thread.sleep(2000);
                    System.out.println("线程1 执行计算任务");
                    semaphore.release();
                }catch (Exception e){


                }



            }
        }.start();


        new Thread(){
            @Override
            public void run() {

                try {
                    Thread.sleep(1000);
                    System.out.println("线程2 执行计算任务");
                    semaphore.release();
                }catch (Exception e){


                }



            }
        }.start();

semaphore.acquire();
        System.out.println("等待1个线程完成任务即可");




    }

}
