package com.qi.demo.Thread;

import java.util.LinkedList;

public class MyQuene {


    private   static   final  int MAX_SIZE=10;



   private LinkedList<String>   queue=new LinkedList<String>();



   public synchronized   void  offer(String element){

       try {
           if (queue.size()==MAX_SIZE){

               wait();

           }
           queue.addLast(element);
           notifyAll();
       }catch (Exception e){

           e.printStackTrace();
       }


   }


   public   synchronized   String   take(){

       String element=null;
       try {

           if (queue.size()==0){

               wait();
           }

           element=   queue.removeLast();

           notifyAll();

       }catch (Exception e){

           e.printStackTrace();
       }


       return element;
   }








    public static void main(String[] args) {
        MyQuene  myQuene=new MyQuene();

 Thread  offerThread=  new  Thread(){
           @Override
           public void run() {

               for (int i=0;i<11;i++){
                   myQuene.offer("String"+"_"+i);
                   System.out.println("当前的假的数据为"+i);

               }

           }
       };



        Thread  takeThread=  new  Thread(){
            @Override
            public void run() {

                for (int i=0;i<11;i++){

                    String  last=   myQuene.take();
                    System.out.println(last);
                }

            }
        };




        offerThread.start();

        takeThread.start();

    }

}
