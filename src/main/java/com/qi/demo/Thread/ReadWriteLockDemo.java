package com.qi.demo.Thread;

import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReadWriteLockDemo {

    public static void main(String[] args) {


        ReentrantReadWriteLock  reentrantReadWriteLock=new ReentrantReadWriteLock();


        reentrantReadWriteLock.writeLock().lock();
        reentrantReadWriteLock.writeLock().unlock();





        reentrantReadWriteLock.readLock().lock();
        reentrantReadWriteLock.readLock().unlock();
    }
}
