package com.qi.demo.Thread;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @description:
 * @author: qi
 * @create: 2019-02-18 13:55
 **/
public class ReentractLockDemo {

    static   volatile  int data=0;
  static   ReentrantLock  reentrantLock=new ReentrantLock();
    public static void main(String[] args) {


        new  Thread(){

            @Override
            public void run() {
                reentrantLock.lock();

                try {
                    reentrantLock.tryLock(10L,TimeUnit.SECONDS);
                }catch (Exception e){
                    e.printStackTrace();
                }

                for (int i=0;i<10;i++){
                    ReentractLockDemo.data++;
                    System.out.println(data);

                }
                reentrantLock.unlock();


            }
        }.start();


        new  Thread(){

            @Override
            public void run() {
//                reentrantLock.lock();
                for (int i=0;i<10;i++){
                    ReentractLockDemo.data++;
                    System.out.println(data);

                }
//                reentrantLock.unlock();

            }
        }.start();




    }
}
