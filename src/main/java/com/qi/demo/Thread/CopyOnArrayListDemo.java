package com.qi.demo.Thread;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @description:
 * @author: qi
 * @create: 2019-02-22 13:32
 **/
public class CopyOnArrayListDemo {

    static List<String>  list=new CopyOnWriteArrayList<String>();


    public static void main(String[] args) {



        list.add("张三");

        list.set(0,"李四");


        Iterator<String>  iterator=list.iterator();

        while (iterator.hasNext()){

            System.out.println(iterator.next());
        }


        System.out.println(list.get(0));


    }
}
