package com.qi.demo.Thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;


public class CachedThreadPoolDemo {


    public static void main(String[] args) {


      ExecutorService executorService = Executors.newCachedThreadPool();


      for (int i=0;i<10;i++){

          executorService.execute(new Runnable() {
              @Override
              public void run() {
                  try {

                      System.out.println("线程池执行异步任务");
                       Thread.sleep(2000);

                  }catch (Exception e){
                      e.printStackTrace();
                  }
              }
          });

      }
    }
}
