package com.qi.demo.Thread;

import java.util.concurrent.CyclicBarrier;

/**
 * @description:
 * @author: qi
 * @create: 2019-02-21 10:52
 **/
public class CyclicBarrierDemo {

    public static void main(String[] args) {


        CyclicBarrier  cyclicBarrier=new CyclicBarrier(3, new Runnable() {
            @Override
            public void run() {


                System.out.println("所有线程都完成了自己的一部分工作  现在可以合并结果了。。。。");
            }
        });

        new Thread(){

            @Override
            public void run() {

                try {
                    System.out.println("线程1做的一部分工作");
                    cyclicBarrier.await();

                    System.out.println("最终合并结果完成 线程1 可以退出");

                }catch (Exception e){
                    e.printStackTrace();
                }


            }
        }.start();



        new Thread(){

            @Override
            public void run() {

                try {
                    System.out.println("线程2做的一部分工作");
                    cyclicBarrier.await();

                    System.out.println("最终合并结果完成 线程2 可以退出");

                }catch (Exception e){
                    e.printStackTrace();
                }


            }
        }.start();





        new Thread(){

            @Override
            public void run() {

                try {
                    System.out.println("线程3做的一部分工作");
                    cyclicBarrier.await();

                    System.out.println("最终合并结果完成 线程3 可以退出");

                }catch (Exception e){
                    e.printStackTrace();
                }


            }
        }.start();

    }




}
