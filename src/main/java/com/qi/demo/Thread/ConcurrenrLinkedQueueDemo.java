package com.qi.demo.Thread;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @description: 线程安全的链表结构无界队列
 * @author: qi
 * @create: 2019-02-22 15:29
 **/
public class ConcurrenrLinkedQueueDemo {


    static  ConcurrentLinkedQueue<String>   queue=  new ConcurrentLinkedQueue<String>();

    public static void main(String[] args) {
        queue.offer("张三");
        queue.offer("李四");
        queue.offer("王五");
        queue.offer("赵六");
        System.out.println(queue.poll());
        System.out.println(queue.peek());
        queue.remove("李四");
        System.out.println(queue);

    }


}
