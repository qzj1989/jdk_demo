package com.qi.demo.List;

import java.util.Stack;

/**
 * @description:
 * @author: qi
 * @create: 2019-02-14 10:07
 **/
public class StackDemo {


    public static void main(String[] args) {
        Stack<String>  stack=new Stack<String>();
        stack.push("张三");
        stack.push("李四");
        stack.push("王五");
        System.out.println(stack.pop());
    }
}
