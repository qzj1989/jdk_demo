package com.qi.demo.List;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @program demo
 * @description:
 * @author: qizhenjiang
 * @create: 2019/02/14 20:57
 */
public class LinkedHashMapDemo {

    public static void main(String[] args) {


        Map<Integer,String>  map=new LinkedHashMap<Integer, String>();


        map.put(2,"张三");
        map.put(1,"李四");
        map.put(3,"王五");
        map.remove(1);
        for (Map.Entry<Integer,String> entry:map.entrySet()){

            System.out.println(entry);

        }

    }
}
